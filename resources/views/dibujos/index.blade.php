@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row mb-5 ml-5">
		<h2 class="display-4">Dibujos ronda {{ $ronda->id }}</h2>
	</div>
	<div class="row">
		@foreach( $arrayDibujos as $dibujo )
			<div class="col-xs-12 col-sm-6 col-md-2 ">
				<a href="{{ url('/dibujos/ver') }}/{{$dibujo->id}}">
				<div style="margin-left: 50px;">
					<div class="btn-group-vertical">

					<img class=" img-thumbnail rounded border" src="{{ asset('assets/imagenes/') }}/{{ $dibujo->imagen }}" style="height:250px"/>
				</a>
					<form action="{{ url('rondas/voto') }}/{{ $dibujo->ronda_id }}/{{ $dibujo->id }}" method="post"> 
				      {{ csrf_field() }}
					      {{-- <div class="form-group text-center">
							<input name="sumaVoto" type="hidden" value="{{ $dibujo->id }}">
					      </div> --}}
							<input type="submit" class=" ml-5 mt-2 btn btn-secondary btn-sm" value="Votar +1"></input>
					</form>
					</div>
				</div>
			</div>


		@endforeach

	</div>
		<a href="{{ url('dibujo/subirDibujo') }}{{--/ {{ $DibujoSeleccionada->id }} --}}" type="button" class=" mt-4 ml-5 btn btn-outline-primary " > Subir dibujo </a>

@endsection