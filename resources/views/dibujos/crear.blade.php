@extends('layouts.master')

@section('titulo')
    Crear
@endsection

@section('contenido')
<div class="row">
  <div class="offset-md-3 col-md-6">
    <div class="card">
      <div class="card-header  text-center">
        Subir Dibujo
      </div>
  {{-- TODO: Abrir el formulario e indicar el método POST --}}
    <form action="{{action('DibujosController@postCrear') }}" method="post" enctype="multipart/form-data"> {{--url('mascotas/crear')--}}
      {{ csrf_field() }}
      <div class="card-body" style="padding:30px">
          <input name="imagen" type="file" class="btn btn-outline-primary btn-block" />
      </div>
      <div class="form-group text-center">
        <button type="submit" class="btn btn-outline-primary " >Subir dibujo</button>
      </div>
    </form>
    </div>
   </div>
</div>
</div>
@endsection