@extends('layouts.master')

@section('titulo')
	Index
@endsection

@section('contenido')

	@if (session('mensaje'))
		<div class="alert alert-success" role="alert">
		  {{ session('mensaje') }}
		</div>
	@endif

	<div class="row">
	{{-- <h3 class="display-4 ">rondas activas </h3> --}}
		{{-- Listado de rondas --}}
		@foreach( $arrayRondas as $ronda )
			<div class="col-xs-12 col-sm-6 col-md-2 ">
				<a href="{{ url('/rondas/ver' ) }}/{{$ronda->id}}">
				<div style="margin-left: 50px;">
					<h4 class="btn btn-primary" >
						Ronda {{ $ronda->id }} :
						{{ $ronda->tema_ronda }}
						
					</h4>
					</a>
				</div>
		@endforeach
	</div>

@endsection