<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@getInicio');

Route::get('rondas', 'RondasController@getTodos');

Route::get('/dibujos/ver/{id}', 'DibujosController@getVer')->where('id','[0-9]+');

Route::get('/rondas/ver/{ronda_id}', 'DibujosController@getVerDibujosRonda')->where('id','[0-9]+');

Route::post('dibujo/subirDibujo', 'DibujosController@postCrear');

Route::post('rondas/voto/{ronda_id}/{id}', 'DibujosController@postVotos');//'DibujosController@postVotos'

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	Route::get('dibujo/subirDibujo', 'DibujosController@getCrear');
	//Route::post('dibujo/sumarVotos/{id}', 'DibujosController@postVotos');
});
