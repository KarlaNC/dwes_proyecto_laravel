<?php

use App\User;
use App\Partida;
use App\Ronda;
use App\Dibujo;
use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    private $arrayDibujos = array(
        array(
            'user_id' => 1,
            'ronda_id' => 1,
            'fecha' => '2020-01-02',
            'votos' => 5,
            'imagen' => 'elfo1.jpg',
        ),
        array(
            'user_id' => 2,
            'ronda_id' => 1,
            'fecha' => '2020-01-05',
            'votos' => 3,
            'imagen' => 'elfo2.jpg',
        ),
        array(
            'user_id' => 3,
            'ronda_id' => 1,
            'fecha' => '2020-01-05',
            'votos' => 4,
            'imagen' => 'elfo3.jpg',
        ),
    );

     private $arrayUsuarios = array(
        array(
            'name' => "karla",
            'email' => "karla@gmail.com",
            'password' => '123',
        ),
        array(
            'name' => "Sergio",
            'email' => "sergio@gmail.com",
            'password' => '123',
        ),
        array(
            'name' => "Lucia",
            'email' => "lucia@gmail.com",
            'password' => '123',
        ),
    );

    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        self::seedUser();
        self::seedPartida();
        self::seedRonda();
        self::seedDibujo();
        $this->command->info('Tablas inicializadas con datos');
    }

    private function seedUser(){
    	DB::table('users')->delete();//Borrado de toda la tabla

        foreach ($this->arrayUsuarios as $user) {
            $u = new User(); //Dibujo::findOrFail($id);
            $u->name =  $user['name'];
            $u->email = $user['email'];
            $u->password = bcrypt($user['password']);
            $u->save();
        }
    }

    private function seedPartida(){
    	DB::table('partidas')->delete();//Borrado de toda la tabla

    	$partida = new Partida();
    	$partida->save();
    }

    private function seedRonda(){
    	DB::table('rondas')->delete();//Borrado de toda la tabla

    	$ronda = new Ronda();
        $ronda->partida_id = Partida::all()->first()->id;
    	$ronda->tema_ronda = "Elfos";
    	$ronda->save();
    }

    private function seedDibujo(){
    	DB::table('dibujos')->delete();

        foreach ($this->arrayDibujos as $dibujo) {
            $d = new Dibujo(); //Dibujo::findOrFail($id);
            $d->user_id =  $dibujo['user_id'];
            $d->ronda_id = $dibujo['ronda_id'];
            $d->fecha = $dibujo['fecha'];
            $d->votos = $dibujo['votos'];
            $d->imagen = $dibujo['imagen'];
            $d->save();
        }
    }
}
