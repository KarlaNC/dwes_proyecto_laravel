<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dibujo;
use App\Ronda;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\DB;

class DibujosController extends Controller
{


    public function getVer($id){ //Con id-nombre
    	$dibujoID = Dibujo::findOrFail($id);
		return view('dibujos.mostrar',  array('dibujoSeleccionado' => $dibujoID));
	}


	public function getVerDibujosRonda($ronda_id){ //Con id-nombre
    	$ronda = Ronda::findOrFail($ronda_id); //Busca en la tabla rondas esa id
    	$dibujos = Dibujo::all()->where('ronda_id', $ronda_id); // Select * from dibujos where ronda_id = $id:

		return view('dibujos.index', array(
			'ronda' => $ronda,
			'arrayDibujos' => $dibujos
		));
	}

	public function getCrear(){
		return view('dibujos.crear');
	}

	public function postCrear(Request $request){

		$dibujo = new Dibujo();

		$dibujo->user_id = 1; 
		$dibujo->ronda_id =  1;//$request->ronda_id;
		$dibujo->fecha = date("Y-m-d");;//$request->fecha;
		$dibujo->votos = 3;
		$dibujo->imagen = $request->imagen->store('','dibujos'); // 

		try {
			$dibujo->save();
			return redirect("/rondas/ver/$dibujo->ronda_id")->with("mensaje", "Dibujo subido con exito!");

		} catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("dibujos")->with("mensaje", "Fallo al subir el dibujo");
		}

		return view('dibujos.crear');
	}

	public function postVotos(Request $request, $ronda_id,$dibujo_id){

		$ronda = Ronda::findOrFail($ronda_id); 
     	$dibujos = Dibujo::all()->where('ronda_id', $ronda_id);
     	
		try{
			DB::table('dibujos')->where('id', $dibujo_id)->increment('votos',1);
			return redirect("/rondas/ver/$ronda_id")->with("mensaje", "Voto realizado con exito!");
		}catch (Exception $ex) { //\Illuminate\Database\QueryException
			return redirect("dibujos")->with("mensaje", "Fallo votar");
		}
	}

}
