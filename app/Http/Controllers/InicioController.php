<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InicioController extends Controller
{
    public function getInicio(){
    	//return return view('home');
    	//No hay pagina de inicio como tal,sino que muestra todas las mascotas
    	return redirect()->action('RondasController@getTodos');
    }
}
