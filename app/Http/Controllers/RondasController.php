<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ronda;
use Illuminate\Support\Facades\Storage;

class RondasController extends Controller
{

    public function getTodos(){
    	$rondas = Ronda::all();
		return view('rondas.index', array('arrayRondas' => $rondas));
	}
}
