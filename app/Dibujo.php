<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dibujo extends Model
{
    public function autor()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
